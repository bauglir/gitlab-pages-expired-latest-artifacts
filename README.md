# GitLab Pages Expired Latest Artifacts Issue

This project shows the issue with expired 'latest' artifacts no longer being
available for viewing through GitLab pages in !1.
